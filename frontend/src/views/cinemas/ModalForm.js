import React, { useState } from "react";
import { Modal, ModalHeader, ModalBody } from "reactstrap";
import { Card, CardHeader, Label, Input, CardBody, Button } from "reactstrap";

const ModalForm = (props) => {
  const initialFormState = {
    name: "",
    description: "",
    location: "",
    seats: null,
  };
  const [cinema, setCinema] = useState(initialFormState);
  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setCinema({ ...cinema, [name]: value });
  };

  const handleSaveCinema = (e) => {
    e.preventDefault();
    if (
      !cinema.name ||
      !cinema.description ||
      !cinema.location ||
      !cinema.seats
    )
      return alert("Incomplete Information");
    props.addCinema(cinema);
    setCinema(initialFormState);
  };

  console.log(cinema);
  return (
    <Modal isOpen={props.showForm} toggle={props.toggleForm}>
      <ModalHeader toggle={props.toggleForm}>
        <h1 className="text-center"> Cinemas Cinemas </h1>
      </ModalHeader>
      <ModalBody>
        <Card>
          <CardBody>
            <Label>Name:</Label>
            <Input
              placeholder="Name of the Cinema"
              name="name"
              type="text"
              value={cinema.name}
              onChange={handleInputChange}
            />
            <Label>Description:</Label>
            <Input
              placeholder="Description of the Cinema"
              name="description"
              type="text"
              value={cinema.description}
              onChange={handleInputChange}
            />
            <Label>Location:</Label>
            <Input
              placeholder="Loation of the Cinema"
              name="location"
              type="text"
              value={cinema.location}
              onChange={handleInputChange}
            />
            <Label>Seats:</Label>
            <Input
              placeholder="Choose your seat"
              name="seats"
              type="number"
              value={cinema.seats}
              onChange={handleInputChange}
            />
          </CardBody>
          <Button onClick={handleSaveCinema}>
            {props.isEditing ? "Edit Question" : "Add Question"}
          </Button>
        </Card>
      </ModalBody>
    </Modal>
  );
};

export default ModalForm;
