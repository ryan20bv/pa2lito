const mongoose = require("mongoose");

const Schema = mongoose.Schema;
const LocationSchema = new Schema(
  {
    name: String,
    location: String,
  },
  { timestamps: true }
);
module.exports = mongoose.model("Location", LocationSchema);
