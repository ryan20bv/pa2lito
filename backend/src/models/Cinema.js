const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const CinemaSchema = new Schema(
  {
    name: String,
    seats: Number,
    description: String,
    location: {
      type: Schema.Types.ObjectId,
      ref: "Location",
    },
  },
  { timestamps: true }
);
module.exports = mongoose.model("Cinema", CinemaSchema);
