import React, { useEffect, useState } from "react";
import SideBar from "../../components/SideBar";
import {
  Container,
  Row,
  Col,
  Card,
  CardHeader,
  Button,
  CardBody,
  Table,
} from "reactstrap";
import LocationRow from "./LocationRow";
import LocationForm from "./LocationForm";
import Loader from "../../components/loader";

const Locations = (props) => {
  const [locations, setLocations] = useState([]);
  const [showForm, setShowForm] = useState(false);
  const [isEditing, setIsEditing] = useState(false);
  const [locationToEdit, setLocationToEdit] = useState({});
  const [indexToEdit, setIndexToEdit] = useState("");
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    setIsLoading(true);

    fetch("http://localhost:4000/admin/locations")
      .then((res) => res.json())
      .then((res) => {
        setLocations(res);
        setIsLoading(false);
      });
  }, [locations]);

  const handleSaveLocation = (name, location) => {
    setIsLoading(true);
    let newLocations = [];

    if (isEditing) {
      let editingName = name;
      let editingLocation = location;

      if (name === "") editingName = locationToEdit.name;
      if (location === "") editingLocation = locationToEdit.location;

      newLocations = locations.map((location, index) => {
        if (indexToEdit === index) {
          location.name = editingName;
          location.location = editingLocation;
        }
        return location;
      });
    } else {
      let apiOptions = {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({
          name,
          location,
        }),
      };

      fetch("http://localhost:4000/admin/addlocation", apiOptions)
        .then((res) => res.json())
        .then((res) => {
          console.log(res);
          setIsLoading(false);
        });
    }

    console.log(newLocations);
    setLocations(newLocations);
    setShowForm(false);
    setLocationToEdit({});
    setIndexToEdit("");
    setIsEditing(false);
  };

  const handleDeleteLocation = (lIndex) => {
    const newLocations = locations.filter((location, index) => {
      return lIndex != index;
    });
    setLocations(newLocations);
  };

  return (
    <>
      <SideBar />
      <div
        className="d-flex justify-content-center"
        style={{ marginLeft: "300px" }}
      >
        <Container className="mt--7" fluid>
          <Row>
            <Card className="col-lg-8 offset-2">
              <CardHeader className="d-flex flex-column justify-content-center align-items-center">
                <h1>Locations</h1>
                <Button onClick={() => setShowForm(!showForm)}>
                  Add Locations
                </Button>
                <LocationForm
                  showForm={showForm}
                  setShowForm={setShowForm}
                  handleSaveLocation={handleSaveLocation}
                  locationToEdit={locationToEdit}
                  isEditing={isEditing}
                />
              </CardHeader>
              <CardBody>
                <Table bordered>
                  <thead>
                    <tr>
                      <th>Name</th>
                      <th>Location</th>
                      <th style={{ width: "40%" }}></th>
                    </tr>
                  </thead>
                  <tbody>
                    {locations.map((location, index) => {
                      return (
                        <LocationRow
                          key={index}
                          index={index}
                          location={location}
                          handleDeleteLocation={() =>
                            handleDeleteLocation(index)
                          }
                          setShowForm={setShowForm}
                          setIsEditing={setIsEditing}
                          setLocationToEdit={setLocationToEdit}
                          setIndexToEdit={setIndexToEdit}
                        />
                      );
                    })}
                  </tbody>
                </Table>
              </CardBody>
            </Card>
          </Row>
        </Container>
      </div>
    </>
  );
};

export default Locations;
