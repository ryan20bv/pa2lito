import React, { useState } from "react";
import SideBar from "../../components/SideBar";
import { Card, CardHeader, Label, Input, CardBody, Button } from "reactstrap";
import ModalForm from "./ModalForm";

const Cinemas = (props) => {
  const details = [
    {
      name: "Trinoma",
      description: "Date with Brandon",
      location: "Quezon City",
      seats: [3, 4, 5],
    },
    {
      name: "SM Megamall",
      description: "Date with Brandon 2",
      location: "EDSA",
      seats: [13, 14, 15],
    },
    {
      name: "SM Aura",
      description: "Date with Brandon 3",
      location: "BGC",
      seats: [3, 4, 5],
    },
  ];

  const [cinemas, setCinemas] = useState(details);

  const [showForm, setShowForm] = useState(false);
  const [isEditing, setIsEditing] = useState(false);

  const addCinema = (cinema) => {
    setShowForm(false);
    setCinemas([...cinemas, cinema]);
  };

  const toggleForm = () => {
    setShowForm(false);
  };

  return (
    <React.Fragment>
      <SideBar />
      <div className="col-lg-6 offset-lg-4 mt-3">
        <Button onClick={() => setShowForm(!showForm)}>Add Cinema</Button>
        <ModalForm
          showForm={showForm}
          toggleForm={toggleForm}
          addCinema={addCinema}
        />
      </div>
    </React.Fragment>
  );
};

export default Cinemas;
