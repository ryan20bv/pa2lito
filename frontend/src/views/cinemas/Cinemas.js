import React, { useState } from "react";
import SideBar from "../../components/SideBar";
import Seats from "./Seats";
import Cinema from "./Cinema";
import ModalForm from "./ModalForm";

const Cinemas = (props) => {
  /*
	Kristel
*/
  const [cinemas, setCinemas] = useState([
    {
      name: "Cinema 1",
      description: "Date with Brandon",
      location: "Greenbelt 5 South Wing",
      //   seats: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
      seats: 20,
    },
    {
      name: "Cinema 2",
      description: "Date with Brenda",
      location: "Greenbelt 1 North Wing",
      //   seats: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
      seats: 15,
    },
    {
      name: "Cinema 3",
      description: "Date with Britney",
      location: "Greenbelt 3 West Wing",
      //   seats: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
      seats: 12,
    },
    {
      name: "Cinema 4",
      description: "Date with Berting",
      location: "Greenbelt 4 East Wing",
      //   seats: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
      seats: 30,
    },
    {
      name: "Cinema 5",
      description: "Date with Bobby",
      location: "Greenbelt 3 Main Wing",
      //   seats: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
      seats: 20,
    },
    {
      name: "Cinema 6",
      description: "Date with Brendon",
      location: "Greenbelt 6 North Wing",
      //   seats: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
      seats: 15,
    },
    {
      name: "Cinema 7",
      description: "Date with Brandon",
      location: "Greenbelt 4 West Wing",
      //   seats: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
      seats: 10,
    },
    {
      name: "Cinema 8",
      description: "Date with Brandon",
      location: "Greenbelt 1 East Wing",
      //   seats: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
      seats: 10,
    },
    {
      name: "Cinema 9",
      description: "Date with Brandon",
      location: "Greenbelt 8 North Wing",
      //   seats: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
      seats: 10,
    },
    {
      name: "Cinema 10",
      description: "Date with Brandon",
      location: "Greenbelt 7 West Wing",
      //   seats: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
      seats: 15,
    },
  ]);

  /*
	Kuya ry
*/
  const [showForm, setShowForm] = useState(false);
  const [isEditing, setIsEditing] = useState(false);
  const [cinemaToEdit, setCinemaToEdit] = useState({});

  const addCinema = (cinema) => {
    setShowForm(false);
    setCinemas([cinema, ...cinemas]);
  };

  const toggleForm = () => {
    setShowForm(false);
  };

  /*
		Ate seth
  */
  const deleteCinema = (index) => {
    const newCinema = cinemas.filter((cinema, qIndex) => {
      return qIndex !== index;
    });
    setCinemas(newCinema);
  };

  const editCinema = (cinema, index) => {
    setShowForm(true);
    setIsEditing(true);
  };

  return (
    <>
      <SideBar />
      <div className="col-lg-9 ml-auto">
        <div className="row">
          <div className="col-lg-12 d-flex justify-content-center align-items-center mt-5">
            <div
              className="shadow"
              style={{
                padding: "12px",
                height: "50vh",
                maxWidth: "80%",
                border: "1px solid black",
                borderRadius: "12px",
                backgroundColor: "#cfe6e5",
              }}
            >
              <div
                className="h-100 w-100"
                style={{
                  padding: "12px",
                  overflowY: "scroll",
                  overflowX: "hidden",
                  scrollSnapType: "y mandatory",
                }}
              >
                <div className="row">
                  {cinemas.map((cinema, index) => (
                    <Cinema
                      index={index}
                      cinema={cinema}
                      key={index}
                      deleteCinema={deleteCinema}
                      editCinema={editCinema}
                      showForm={showForm}
                      setShowForm={setShowForm}
                    />
                  ))}
                </div>
              </div>
              <button
                onClick={() => setShowForm(!showForm)}
                className="btn btn-primary shadow-sm"
                style={{
                  backgroundColor: "#213343",
                }}
              >
                {"ADD +"}
              </button>
            </div>
          </div>
        </div>
        <Seats />
      </div>

      <ModalForm
        addCinema={addCinema}
        toggleForm={toggleForm}
        showForm={showForm}
        isEditing={isEditing}
      />
    </>
  );
};

export default Cinemas;
