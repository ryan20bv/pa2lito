const express = require("express");
const LocationRouter = express.Router();
const LocationModel = require("../models/Location");

// add location
LocationRouter.post("/addlocation", async (req, res) => {
  //   console.log(req.body);
  try {
    let location = LocationModel({
      name: req.body.name,
      location: req.body.location,
    });
    location = await location.save();
    res.send(location);
  } catch (e) {
    res.status(401).send("Bad Request. Please try again");
  }
});
// get all location
LocationRouter.get("/alllocation", async (req, res) => {
  try {
    const locations = await LocationModel.find();
    res.send(locations);
  } catch (e) {
    res.status(401).send("Bad Request.");
  }
});

//  delete Location
LocationRouter.delete("/deletelocation", async (req, res) => {
  try {
    const location = await LocationModel.findByIdAndDelete(req.body.id);
    res.send(location);
  } catch (e) {
    res.status(401).send("Bad Request.");
  }
});

//  update location
LocationRouter.patch("/updatelocation", async (req, res) => {
  try {
    const updates = {
      name: req.body.name,
      location: req.body.location,
    };
    let location = await LocationModel.findByIdAndUpdate(req.body.id, updates, {
      new: true,
    });
    res.send(location);
  } catch (e) {
    res.status(401).send("Bad Request.");
  }
});
module.exports = LocationRouter;
