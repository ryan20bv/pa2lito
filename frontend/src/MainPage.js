import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import Loader from "./components/loader";

//import our pages
const Cinemas = React.lazy(() => import("./views/cinemas/Cinemas"));
const Locations = React.lazy(() => import("./views/locations/Locations"));
const Movies = React.lazy(() => import("./views/movies/Movies"));

const MainPage = (props) => {
  return (
    <BrowserRouter>
      {/* React.Suspnse is require if have reactlazy */}
      <React.Suspense fallback={Loader}>
        <Switch>
          <Route path="/cinemas" render={(props) => <Cinemas {...props} />} />
          <Route
            path="/locations"
            render={(props) => <Locations {...props} />}
          />

          <Route path="/movies" render={(props) => <Movies {...props} />} />
        </Switch>
      </React.Suspense>
    </BrowserRouter>
  );
};

export default MainPage;

// BrowserRouter
//     /Cinemas
//     /Movies
//     /Locations

// HashRouter   - friendlier to all browser
//     /#/Cinemas
//     /#/Movies
//     /#/Locations
