import React, { useState } from "react";
import {
  Modal,
  ModalHeader,
  ModalBody,
  FormGroup,
  Label,
  Input,
  Button,
} from "reactstrap";

const LocationForm = ({
  showForm,
  setShowForm,
  handleSaveLocation,
  locationToEdit,
  isEditing,
}) => {
  const [name, setName] = useState("");
  const [location, setLocation] = useState("");

  return (
    <Modal isOpen={showForm} toggle={() => setShowForm(!showForm)}>
      <ModalHeader toggle={() => setShowForm(!showForm)}>
        {isEditing ? "Edit Location" : "Add Location"}
      </ModalHeader>
      <ModalBody>
        <FormGroup>
          <Label>Name:</Label>
          <Input
            placeholder="Name"
            onChange={(e) => setName(e.target.value)}
            defaultValue={isEditing ? locationToEdit.name : ""}
          />
        </FormGroup>
        <FormGroup>
          <Label>Location:</Label>
          <Input
            placeholder="Location"
            onChange={(e) => setLocation(e.target.value)}
            defaultValue={isEditing ? locationToEdit.location : ""}
          />
        </FormGroup>
        <Button
          color="info"
          block
          onClick={() => {
            handleSaveLocation(name, location);
            setName("");
            setLocation("");
          }}
        >
          {isEditing ? "Update Location" : "Save Location"}
        </Button>
      </ModalBody>
    </Modal>
  );
};

export default LocationForm;
