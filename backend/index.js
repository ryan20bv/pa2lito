// call express
const express = require("express");
//  create an instance of express
const app = express();
//  call cors
const cors = require("cors");

// mongoose
const mongoose = require("mongoose");

// call config file
const config = require("./src/config");
// allow parsing of data from URL

app.use(express.urlencoded({ extended: false }));
// allow parsing of data from a form

app.use(express.json());

// allow cors
app.use(cors());

// connect to database

mongoose
  .connect(config.databaseURL, {
    useNewUrlParser: true,
    useUnifiedTopology: true, //for syntax options
    useFindAndModify: false, //find by id options
  })
  .then(() => {
    console.log("Remote DATABASE connection successful");
  });

app.listen(config.PORT, () => {
  console.log(`Listening on Port ${config.PORT}`);
});

//location API
const locationRoutes = require("./src/routes/LocationRoutes");
app.use("/admin", locationRoutes);

//Movie API

const movieRoutes = require("./src/routes/MovieRoutes");
app.use("/admin", movieRoutes);
// Cinema API
const cinemaRoutes = require("./src/routes/CinemaRoutes");
app.use("/admin", cinemaRoutes);

const scheduleRoutes = require("./src/routes/CinemaRoutes");
app.use("/admin", scheduleRoutes);

const bookingRoutes = require("./src/routes/BookingRoutes");
app.use("/admin", bookingRoutes);

const userRoutes = require("./src/routes/UserRoutes");
app.use("/", userRoutes);
