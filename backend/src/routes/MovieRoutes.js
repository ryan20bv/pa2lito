const express = require("express");
const MovieRouter = express.Router();
const MovieModel = require("../models/Movie");

// all movie
MovieRouter.post("/addmovie", async (req, res) => {
  try {
    let movie = MovieModel({
      title: req.body.title,
      director: req.body.director,
      artists: req.body.artists,
      coverImg: req.body.coverImg,
      preview: req.body.preview,
      summary: req.body.summary,
      runTime: req.body.runTime,
    });

    movie = await movie.save();

    res.send(movie);
  } catch (e) {
    res.status(401).send("Bad Request");
  }
});

// all movie
MovieRouter.get("/allmovie", async (req, res) => {
  try {
    const movies = await MovieModel.find();
    res.send(movies);
  } catch (e) {
    res.status(401).send("Bad Request");
  }
});

// delete Movie
MovieRouter.delete("/deletemovie", async (req, res) => {
  try {
    const movie = await MovieModel.findByIdAndDelete(req.body.id);
    res.send(movie);
  } catch (e) {
    res.status(401).send("Bad Request");
  }
});
//  update Movie
MovieRouter.patch("/updatemovie", async (req, res) => {
  try {
    const updates = {
      title: req.body.title,
      director: req.body.director,
      artists: req.body.artists,
      coverImg: req.body.coverImg,
      preview: req.body.preview,
      summary: req.body.summary,
      runTime: req.body.runTime,
    };

    let movie = await MovieModel.findByIdAndUpdate(req.body.id, updates, {
      new: true,
    });
    res.send(movie);
  } catch (e) {
    res.status(401).send("Bad Request");
  }
});

module.exports = MovieRouter;
