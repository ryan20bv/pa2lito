import React from "react";

const Cinema = ({ cinema, deleteCinema, index, editCinema }) => {
  return (
    <React.Fragment>
      <div className="col-lg-6 my-2">
        <div
          className="card"
          style={{
            backgroundColor: "#325453",
            width: "100%",
          }}
        >
          <div className="card-header">
            <strong
              style={{
                color: "#2cc6c1",
              }}
            >
              {cinema.name}
            </strong>
          </div>
          <div className="card-body">
            <p className="text-white">{cinema.description}</p>
            <p className="text-white">Location: {cinema.location}</p>
            <p className="text-white">Seats: {cinema.seats}</p>
          </div>
          <div className="card-footer">
            <button
              className="btn btn-info"
              onClick={() => editCinema(cinema, index)}
            >
              &#9998;
            </button>{" "}
            <button
              className="btn btn-danger"
              onClick={() => deleteCinema(index)}
            >
              &times;
            </button>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

export default Cinema;
