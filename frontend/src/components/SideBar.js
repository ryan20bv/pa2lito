import React from "react";
import Sidebar from "react-sidebar";
import { ListGroup, ListGroupItem } from "reactstrap";
import { Link } from "react-router-dom";

const navItems = [
  {
    name: "Home",
    link: "/home",
  },
  {
    name: "Users",
    link: "/users",
  },
  {
    name: "Locations",
    link: "/location",
  },
  {
    name: "Cinemas",
    link: "/cinemas",
  },
  {
    name: "Movies",
    link: "/movies",
  },
];

const SideBar = (props) => {
  return (
    <Sidebar
      styles={{
        sidebar: {
          backgroundColor: "#2cc6c1",
          position: "fixed",
        },
      }}
      docked
      sidebar={
        <div
          className="d-flex flex-column justify-content-center align-items-center vh-100"
          style={{
            width: 300,
          }}
        >
          <h1>Cinema Code</h1>
          <ListGroup style={{ width: "100%" }}>
            {navItems.map((nav, index) => (
              <Link
                key={index}
                to={nav.link}
                style={{
                  textDecoration: "none",
                  color: "black",
                }}
              >
                <ListGroupItem className="rounded-0 border-white border-left-0 text-center">
                  {nav.name}
                </ListGroupItem>
              </Link>
            ))}
          </ListGroup>
        </div>
      }
    />
  );
};
export default SideBar;
