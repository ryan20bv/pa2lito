const express = require('express');

const ScheduleRouter = express.Router();

const ScheduleModel = require('../models/Schedule');

// add Schedule
ScheduleRouter.post('/addschedule', async (req, res) => {
  try {
    let schedule = ScheduleModel({
      movie: req.body.movie,
      startTime: req.body.startTime,
      endTime: req.body.endTime,
      date: req.body.date,
      seats: req.body.seats,
      price: req.body.price,
      cinema: req.body.cinema
    });

    schedule = await schedule.save();
    res.send(schedule);

  } catch (e) {
    res.status(401).send('Bad Request. Please try again');
  }
})

// get all schedule
ScheduleRouter.get('/schedules', async (req, res) => {
  try {
    const schedules = await ScheduleModel.find();
    res.send(schedules);
  } catch (e) {
    res.status(401).send('Bad Request. Please try again');
  }
});

// delete schedule
ScheduleRouter.delete('/deleteschedule', async (req, res) => {
  try {
    const schedule = await ScheduleModel.findByIdAndDelete(req.body.id);
    res.send(schedule);
  } catch (e) {
    res.status(401).send('Bad Request. Please try again');
  }
});

// update schedule
ScheduleRouter.patch('/updateschedule', async (req, res) => {
  try {
    const updates = {
      movie: req.body.movie,
      startTime: req.body.startTime,
      endTime: req.body.endTime,
      date: req.body.date,
      seats: req.body.seats,
      price: req.body.price,
      cinema: req.body.cinema
    }
    let schedule = await ScheduleModel.findByIdAndUpdate(req.body.id, updates, {
      new: true
    });
    res.send(schedule);
  } catch (e) {
    res.status(401).send('Bad Request. Please try again');
  }
})

module.exports = ScheduleRouter;