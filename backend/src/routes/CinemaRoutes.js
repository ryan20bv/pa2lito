const express = require("express");

const CinemaRouter = express.Router();

const CinemaModel = require("../models/Cinema");

//add cinema
CinemaRouter.post("/addcinema", async (req, res) => {
  try {
    let cinema = CinemaModel({
      name: req.body.name,
      description: req.body.description,
      location: req.body.location,
      seats: req.body.seats,
    });

    cinema = await cinema.save();
    res.send(cinema);
  } catch (e) {
    res.status(401).send("Bad Request. Please try again");
  }
});

//get all cinema

CinemaRouter.get("/cinemas", async (req, res) => {
  try {
    const cinemas = await CinemaModel.find();
    res.send(cinemas);
  } catch (e) {
    res.status(401).send("Bad Request. Please try again");
  }
});

//delete

CinemaRouter.delete("/deletecinema", async (req, res) => {
  try {
    cinema = await CinemaModel.findByIdAndDelete(req.body.id);
    res.send(cinema);
  } catch (e) {
    res.status(401).send("Bad Request. Please try again");
  }
});

//update
CinemaRouter.patch("/updatecinema", async (req, res) => {
  try {
    const updates = {
      name: req.body.name,
      description: req.body.description,
      location: req.body.location,
      seats: req.body.seats,
    };
    let cinema = await CinemaModel.findByIdAndUpdate(req.body.id, updates, {
      new: true,
    });
    res.send(cinema);
  } catch (e) {
    res.status(401).send("Bad Request. Please try again");
  }
});

module.exports = CinemaRouter;
