const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const ScheduleSchema = new Schema(
  {
    movie: {
      type: Schema.Types.ObjectId,
      ref: "Movie",
    },
    cinema: {
      type: Schema.Types.ObjectId,
      ref: "Cinema",
    },
    startTime: String,
    endTime: String,
    date: String,
    seats: Number,
    price: Number,
  },
  { timestamps: true }
);
module.exports = mongoose.model("Schedule", ScheduleSchema);
