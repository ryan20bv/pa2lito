const express = require("express");
const BookingRouter = express.Router();
const BookingModel = require("../models/Booking");

BookingRouter.post("/addbooking", async (req, res) => {
  try {
    let booking = BookingModel({
      schedule: req.body.scheduleId,
      user: req.body.userId,
      payment: req.body.payment,
      amount: req.body.amount,
    });
    booking = await booking.save();
    res.send(booking);
  } catch (e) {
    res.status(401).send("Bad Request. Please try again");
  }
});

BookingRouter.get("/bookings", async (req, res) => {
  try {
    const booking = await BookingModel.find();
    res.send(booking);
  } catch (e) {
    res.status(401).send("Bad Request. Please try again");
  }
});

BookingRouter.delete("/deletebooking", async (req, res) => {
  try {
    const booking = await BookingModel.findByIdAndDelete(req.body.id);
    res.send(booking);
  } catch (e) {
    res.status(401).send("Bad Request. Please try again");
  }
});

BookingRouter.patch("/updatebooking", async (req, res) => {
  try {
    const updates = {
      schedule: req.body.scheduleId,
      user: req.body.userId,
      status: req.body.status,
      payment: req.body.payment,
      amount: req.body.amount,
    };

    let booking = await BookingModel.findByIdAndUpdate(req.body.id, updates, {
      new: true,
    });
    res.send(booking);
  } catch (e) {
    res.status(401).send("Bad Request. Please try again");
  }
});

module.exports = BookingRouter;
