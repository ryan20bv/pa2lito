const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const MovieSchema = new Schema(
  {
    title: String,
    director: String,
    artists: [
      {
        type: String,
      },
    ],
    coverImg: String,
    preview: String,
    summary: String,
    runTime: Number,
    isShowing: {
      type: Boolean,
      default: false,
    },
    isComingSoon: {
      type: Boolean,
      default: false,
    },
  },
  { timestamps: true }
);
module.exports = mongoose.model("Movie", MovieSchema);
